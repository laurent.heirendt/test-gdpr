# frozen_string_literal: true

# Note: keep the format of NUMBER.NUMBER.NUMBER (where NUMBER is [0-9]+);
# Otherwise Gitlab-CI could fail to detect the version (see .gitlab-ci.yml) and auto-tag the job
module Theme
    VERSION = "0.3.18"
end
