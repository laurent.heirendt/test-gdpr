var _paq = window._paq || _paq || [];

// convert days to milliseconds
settings.cookieTimeout = settings.expires * 86400 * 1000;

/*
 * loadMatomo: load the tracking code of the analytics instance
 */
function loadMatomo() {
    if (settings.siteID > 0) {
        var u = settings.matomoURL;
        _paq.push(['setSiteId', settings.siteID]);
        _paq.push(['setTrackerUrl', u + 'matomo.php']);
        _paq.push(['setDoNotTrack', 1]);
        _paq.push(['trackPageView']);
        _paq.push(['setIgnoreClasses', ['no-tracking', 'colorbox']]);
        _paq.push(['enableLinkTracking']);
        var d = document,
            g = d.createElement('script'),
            s = d.getElementsByTagName('script')[0];
        g.type = 'text/javascript';
        g.defer = true;
        g.async = true;
        g.src = u + 'matomo.js';
        s.parentNode.insertBefore(g, s);
        setResultText(true);
    }
}

/*
 * setResultText: change the text in the footer
 */
function setResultText(acceptCookies) {
    if (acceptCookies === true) {
        $('#gdpr-result-text').html(settings.accept_all_text);
    } else {
        $('#gdpr-result-text').html(settings.only_necessary_text);
    }
}

/*
 * showBanner: show the banner
 */
function showBanner() {
    var popup = $('#lap-cookies-banner');
    popup.hide();
    popup.slideDown('slow');
}

/*
 * hideBanner: show the banner
 */
function hideBanner() {
    var popup = $('#lap-cookies-banner');
    setTimeout(() => { popup.slideUp('slow'); }, settings.timeout_hidebanner);
}

/*
 * clearCookies: clear all cookies
 */
function clearCookies() {
    var originOfTime = new Date(0);
    var hostname = document.location.hostname.replace(/^www\./, ''),
        commonSuffix = '; expires= ' + originOfTime + '; path=/';

    // remove the analytics cookies
    var cookies = document.cookie.split('; ');
    for (var i in cookies) {
        var name = cookies[i].split('=')[0];
        if (name.startsWith('_pk_') || name.startsWith('MATOMO_SESSID')) {
            document.cookie = name + '=; ' + commonSuffix;
        }
    }

    setResultText(false);
}

/*
 * setCookie: create/update cookie
 */
function setCookie(name, value) {
    var date = new Date();
    date.setTime(date.getTime() + settings.cookieTimeout);

    document.cookie = name + '=' + value + ';expires=' + date.toGMTString() + ';path=/';
}

function isCookieSetTo(val) {
    return document.cookie.indexOf(settings.cookieName + '=' + val) > -1;
}

/*
 * hasConsent: check if user gave consent
 */
function hasConsent() {
    if (isCookieSetTo(1)) {
        setResultText(true);
        return true;
    } else if (isCookieSetTo(0)) {
        setResultText(false);
        return false;
    }
    return null;
}

/*
 * accept: accept the cookies
 */
function accept() {
    // action
    loadMatomo();

    // cookie management
    setCookie(settings.cookieName, 1);

    // feedback
    setResultText(true);

    // style
    $('.lap-accept').addClass('selected');
    $('.lap-refuse').removeClass('selected');

    // hide banner
    hideBanner();
}

/*
 * accept: refuse the cookies
 */
function refuse(doNotTrack) {
    // action
    clearCookies();

    // cookie management
    // only set the refusal cookie if actually allowed (i.e. doNotTrack = false)
    if (doNotTrack === false) {
        setCookie(settings.cookieName, 0);
    }

    // feedback
    setResultText(false);

    // style
    $('.lap-refuse').addClass('selected');
    $('.lap-accept').removeClass('selected');

    // hide banner
    hideBanner();
}


/*
 * main function
 */
$(document).ready(function() {
    // accept
    $('.lap-accept').click(function(e) {
        e.preventDefault(true);
        accept();
    });

    // refuse
    $('.lap-refuse').click(function(e) {
        e.preventDefault(true);
        refuse(false);
    });

    // detect if the visitor is a bot or not
    // prevent search engine t otake the cookie alert message as main content
    var isBot = settings.bots.test(navigator.userAgent);

    // check if DoNotTrack is active
    var dnt = navigator.doNotTrack || navigator.msDoNotTrack || window.doNotTrack;
    var isToTrack = (dnt !== null && dnt !== undefined) ? (dnt && dnt !== 'yes' && dnt !== 1 && dnt !== '1') : true;
    var doNotTrack = false;

    // do nothing if it is a bot or if DoNotTrack is active
    if (isBot || !isToTrack || hasConsent() === false) {
        doNotTrack = true;
        refuse(doNotTrack);
    }

    if (doNotTrack === false) {
        if (hasConsent() === true) {
            // user has already given consent to use cookies to tracking
            accept();
        } else if (hasConsent() === false || hasConsent() === null) {
            // all other cases considered, show banner
            showBanner();
        }
    }
});